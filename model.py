"""
Kevin Gonzalez 8-937-655
Sergio Allard 9-917-2244
Gabriel Soto 8-892-791
"""



import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import sys



class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1209, 393)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 561, 301))
        self.groupBox.setObjectName("groupBox")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(10, 20, 271, 51))
        self.label.setObjectName("label")
        self.textEdit = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit.setGeometry(QtCore.QRect(410, 30, 111, 31))
        self.textEdit.setObjectName("textEdit")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(10, 90, 321, 41))
        self.label_2.setObjectName("label_2")
        self.textEdit_2 = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit_2.setGeometry(QtCore.QRect(410, 90, 111, 31))
        self.textEdit_2.setObjectName("textEdit_2")
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setGeometry(QtCore.QRect(10, 150, 361, 51))
        self.label_3.setObjectName("label_3")
        self.textEdit_3 = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit_3.setGeometry(QtCore.QRect(410, 160, 111, 31))
        self.textEdit_3.setObjectName("textEdit_3")
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setGeometry(QtCore.QRect(10, 220, 381, 51))
        self.label_4.setObjectName("label_4")
        self.textEdit_4 = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit_4.setGeometry(QtCore.QRect(410, 230, 111, 31))
        self.textEdit_4.setObjectName("textEdit_4")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(700, 70, 471, 191))
        self.groupBox_2.setObjectName("groupBox_2")
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(10, 90, 131, 16))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.groupBox_2)
        self.label_6.setGeometry(QtCore.QRect(10, 40, 141, 16))
        self.label_6.setObjectName("label_6")
        self.textEdit_5 = QtWidgets.QTextEdit(self.groupBox_2)
        self.textEdit_5.setGeometry(QtCore.QRect(180, 40, 111, 21))
        self.textEdit_5.setObjectName("textEdit_5")
        self.textEdit_6 = QtWidgets.QTextEdit(self.groupBox_2)
        self.textEdit_6.setGeometry(QtCore.QRect(180, 90, 111, 21))
        self.textEdit_6.setObjectName("textEdit_6")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(1060, 270, 113, 32))
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(self.calcular)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Sitema Regulador de Insulina y Glucosa en Sangre"))
        self.groupBox.setTitle(_translate("MainWindow", "Valores de Sensibilidad"))
        self.label.setText(_translate("MainWindow",
                                      "Sensibilidad de la actividad de la insulinasa<br> a una concentración elevada de insulina."))
        self.label_2.setText(_translate("MainWindow",
                                        "Sensibilidad de la producción de insulina pancreática <br> a una concentración elevada de glucosa"))
        self.label_3.setText(_translate("MainWindow",
                                        "Sensibilidad combinada del almacenamiento de glucógeno <br> hepático y la utilización de glucosa en los tejidos <br> a una concentración elevada de insulina"))
        self.label_4.setText(_translate("MainWindow",
                                        "Sensibilidad combinada del almacenamiento de glucógeno <br> hepático y la utilización de glucosa en los tejidos <br> a una concentración elevada de glucosa"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Valores Iniciales"))
        self.label_5.setText(_translate("MainWindow", "Glucosa en Sangre"))
        self.label_6.setText(_translate("MainWindow", "Insulina en Sangre"))
        self.pushButton.setText(_translate("MainWindow", "Calcular"))

    def calcular(self):
        plt.close('all')
        alfa = float(self.textEdit.toPlainText())
        beta = float(self.textEdit_2.toPlainText())
        gamma = float(self.textEdit_3.toPlainText())
        delta = float(self.textEdit_4.toPlainText())
        p = float(self.textEdit_5.toPlainText())
        q = float(self.textEdit_6.toPlainText())

        def model(G, t):
            return [p - (alfa * G[0]) + (beta * G[1]), q - (gamma * G[0]) - (delta * G[1])]

        ts = np.linspace(0, 60, 500)

        P0 = [p, q]

        Ps = odeint(model, P0, ts)

        insulin = Ps[:, 0]
        glucose = Ps[:, 1]
        plt.figure(111)
        plt.subplot(1, 2, 1)
        plt.plot(ts, insulin, "r-", label="Insulina")
        plt.plot(ts, glucose, "b-", label="Glucosa")
        plt.xlabel("Tiempo (horas)")
        plt.ylabel("Consentración en Sangre")
        plt.legend()

        plt.subplot(1, 2, 2)
        plt.plot(insulin, glucose, "b.")
        plt.xlabel("Insulina")
        plt.ylabel("Glucosa")
        plt.title("Diagrama de Estado-Fase ")

        plt.subplots_adjust(left=0.125, right=0.9, bottom=0.06, top=0.9, wspace=0.9, hspace=0.9)
        plt.show()



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()

    sys.exit(app.exec_())